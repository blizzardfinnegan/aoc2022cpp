#include <iostream>
#include <fstream>
#include <string>
#include <map>

//This is a port of my code from AoC 2022 in Java, over to C++.
int main(void)
{
	//Current line in the file
	std::string rawInput;

	//Map containing all final values (similar to Java's TreeMap)
	std::map<int, int> elfMap;

	//working values for the elf number and calorie count
	int elfNumber = 1;
	int calorieCount = 0;

	//File stream; do not try to directly interact with, use the rawInput variable instead
	std::ifstream readFile ("data/input.txt", std::ios::in);

	//As long as there's a new file in the readFile stream, send that line to the 
	//rawInput string for storage, then run everything in the while loop
	while(getline(readFile, rawInput))
	{
		//If the string isn't empty, we know it will be an integer. Convert it to 
		//an int, then add it to the running total in calorieCount
		if (!(rawInput.empty())) 
		{
			calorieCount += std::stoi(rawInput);
		}
		//If the string is empty, then its the end of the elf. Add both the number
		//of the elf and the calorie count to the map (which will auto-organise on insert).
		else 
		{
			elfMap.insert({calorieCount,elfNumber});
			calorieCount = 0;
			elfNumber++;
		}
	}

	//This is a temp line for part2
	//for(auto iterator = elfMap.rbegin(); iterator != elfMap.rend(); ++iterator)
	//{
	//	std::cout << "Key:" << iterator->first << "; Value:" << iterator->second << std::endl;
	//}
	
	//Final print
	std::cout << "Elf #" << elfMap.rbegin()->second << " has the most calories, at ";
	std::cout << elfMap.rbegin()->first << std::endl;
	//syntax note:
	//elfMap.rbegin() is an iterator that starts at the end of the map (where the highest
	//key-value pair is stored). The arrow, then first or second refers to which half is 
	//being read. (Second is the value, first is the key)

	return 0;
}
